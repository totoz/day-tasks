export interface ILabelStore {
  labels: ILabel[]
  SET_LABELS: (labels: ILabel[]) => void
  PUSH_LABEL: (label: ILabel) => void
  REMOVE_LABEL: (index: number) => void
  fetchLabels: () => Promise<void>
  createLabel: (label: ILabel) => Promise<void>
  deleteLabel: (labelId: string) => Promise<void>
}

export interface ITasksStore {
  tasks: ITask[]
  SET_TASKS: (tasks: ITask[]) => void
  PUSH_TASK: (task: ITask) => void
  REMOVE_TASK: (index: number) => void
  fetchTasks: () => Promise<void>
  createTask: (task: ITask) => Promise<void>
  deleteTask: (taskId: string) => Promise<void>
}

export interface ILabel {
  id?: string
  name: string
  color: string
}

export interface ITask {
  id?: string
  title: string
  task: string
  start: string
  end: string
  labels: ILabel[]
}
