import Vue from 'vue'
import notification from 'vue-notification'

Vue.use(notification)

// @ts-ignore
export default (context, inject) => {
  inject('notify', Vue.notify)
}
