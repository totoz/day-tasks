import firebase from 'firebase/app'
import 'firebase/firestore'

const config = {
  apiKey: 'AIzaSyC1XbzZ4PlhJMqHlQdq_mH5kR8V8k-JCiQ',
  authDomain: 'day-tasks-cc04a.firebaseapp.com',
  databaseURL: 'https://day-tasks-cc04a.firebaseio.com',
  projectId: 'day-tasks-cc04a',
  storageBucket: 'day-tasks-cc04a.appspot.com',
  messagingSenderId: '431858560099',
  appId: '1:431858560099:web:89d9af77f37650d8201c56',
  measurementId: 'G-2XQBHRHN49',
}
if (!firebase.apps.length) {
  firebase.initializeApp(config)
}
const fireDb = firebase.firestore()

export { fireDb }
