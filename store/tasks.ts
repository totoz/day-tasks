import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { fireDb } from '~/plugins/firebase'
import { ITasksStore, ITask } from '~/types/store'

@Module({ stateFactory: true, namespaced: true, name: 'tasks' })
export default class TasksModule extends VuexModule implements ITasksStore {
  tasks: Array<ITask> = []

  @Mutation
  SET_TASKS(tasks: Array<ITask>) {
    this.tasks = tasks
  }

  @Mutation
  PUSH_TASK(task: ITask) {
    this.tasks.push(task)
  }

  @Mutation
  REMOVE_TASK(index: number) {
    this.tasks.splice(index, 1)
  }

  @Action
  async fetchTasks() {
    const querySnapshot = await fireDb.collection('tasks').get()
    let tasks: Array<ITask> = []
    querySnapshot.forEach((doc) => {
      let data = <ITask>doc.data()
      data = Object.assign(data, { id: doc.id })
      tasks.push(data)
    })
    this.context.commit('SET_TASKS', tasks)
  }

  @Action
  async createTask(task: ITask) {
    const docRef = await fireDb.collection('tasks').add(task)
    this.context.commit('PUSH_TASK', Object.assign(task, { id: docRef.id }))
  }

  @Action
  async deleteTask(taskId: string) {
    await fireDb.collection('tasks').doc(taskId).delete()
    this.context.commit(
      'REMOVE_TASK',
      this.tasks.findIndex((task) => {
        return task.id == taskId
      })
    )
  }
}
