import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { fireDb } from '~/plugins/firebase'
import { ILabelStore, ILabel } from '~/types/store'

@Module({ stateFactory: true, namespaced: true, name: 'labels' })
export default class LabelModule extends VuexModule implements ILabelStore {
  labels: Array<ILabel> = []

  @Mutation
  SET_LABELS(labels: Array<ILabel>) {
    this.labels = labels
  }

  @Mutation
  PUSH_LABEL(label: ILabel) {
    this.labels.push(label)
  }

  @Mutation
  REMOVE_LABEL(index: number) {
    this.labels.splice(index, 1)
  }

  @Action
  async fetchLabels() {
    const querySnapshot = await fireDb.collection('labels').get()
    let labels: Array<ILabel> = []
    querySnapshot.forEach((doc) => {
      let data = <ILabel>doc.data()
      data = Object.assign(data, { id: doc.id })
      labels.push(data)
    })
    this.context.commit('SET_LABELS', labels)
  }

  @Action
  async createLabel(label: ILabel) {
    const docRef = await fireDb.collection('labels').add(label)
    this.context.commit('PUSH_LABEL', Object.assign(label, { id: docRef.id }))
  }

  @Action
  async deleteLabel(labelId: string) {
    await fireDb.collection('tasks').doc(labelId).delete()
    this.context.commit(
      'REMOVE_TASK',
      this.labels.findIndex((label) => {
        return label.id == labelId
      })
    )
  }
}
