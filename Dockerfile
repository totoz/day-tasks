FROM node:12-alpine as builder
WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn --frozen-lockfile
COPY . .
RUN yarn export
RUN npm prune --production


FROM yobasystems/alpine-caddy
WORKDIR /srv
COPY --from=builder /usr/src/app/dist ./todo