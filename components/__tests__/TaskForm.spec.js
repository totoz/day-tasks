import Vue from "vue";
import Vuetify from "vuetify";
import TaskForm from "@/components/TaskForm.vue";
import { shallowMount } from "@vue/test-utils";

Vue.use(Vuetify);

let wrapper = null;
let vuetify = new Vuetify();

describe("TaskForm.vue", () => {
  beforeEach(() => {
    wrapper = shallowMount(TaskForm, {
      vuetify,
    });
  });

  afterEach(() => {
    wrapper.destroy();
  });

  it("should render the vue instance", () => {
    expect(wrapper.findComponent(TaskForm).vm).toBeTruthy();
  });

  it("should show the title input when text area focus", () => {
    const textArea = wrapper.find("[data-testid='task-textarea']");
    textArea.vm.$emit("focus");
    expect(wrapper.vm.$data.showTitle).toBeTruthy();
  });

  it("should hide the title input when click outside the card ", () => {
    wrapper.vm.$data.showTitle = true;
    const card = wrapper.find("[data-testid='taskform-card']");
    card.vm.$on("outside", wrapper.vm.hideTitle);
    card.vm.$emit("outside");
    expect(wrapper.vm.$data.showTitle).toBeFalsy();
  });

  it("should open the add task dialog", () => {
    const addTaskBtn = wrapper.find("[data-testid='addtask-btn']");
    addTaskBtn.vm.$emit("click");
    expect(wrapper.vm.$data.addDialog).toBeTruthy();
  });

  it("should close the add task dialog", () => {
    const addTaskBtn = wrapper.find("[data-testid='addtask-btn']");
    addTaskBtn.vm.$emit("click");
    wrapper.vm.close();
    expect(wrapper.vm.$data.addDialog).toBeFalsy();
  });

  it("should reset the task from inputs", () => {
    wrapper.vm.$data.title = "Title";
    wrapper.vm.$data.task = "Task";
    wrapper.vm.resetForm();
    expect(wrapper.vm.$data.title).toMatch("");
    expect(wrapper.vm.$data.task).toMatch("");
  });
});
