import Vue from 'vue'
import Vuetify from 'vuetify'
import Task from '@/components/Task.vue'
import Vuex from 'vuex'
import { getModule } from 'vuex-module-decorators'
import TasksStore from '~/store/modules/tasks'
import { shallowMount, createLocalVue } from '@vue/test-utils'

Vue.use(Vuetify)

const localVue = createLocalVue()
localVue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    tasks: TasksStore,
  },
})

const tasksStore = getModule(TasksStore, store)

let wrapper = null
let vuetify = new Vuetify()

const removeTask = jest.spyOn(
  Task.prototype.constructor.options.methods,
  'removeTask'
)

describe('TaskForm.vue', () => {
  beforeEach(() => {
    wrapper = shallowMount(Task, {
      propsData: {
        task: {
          id: '123',
          title: 'Tarea 1',
          task: 'Esta es una tarea',
          start: '12:00',
          end: '14:00',
          labels: [],
        },
      },
      vuetify,
      localVue,
      store,
    })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('should render the vue instance', () => {
    expect(wrapper.findComponent(Task).vm).toBeTruthy()
  })

  it('should remove the task', async () => {
    const removeTaskBtn = wrapper.find("[data-testid='removeTask-btn']")
    removeTaskBtn.vm.$emit('click')
    expect(removeTask).toHaveBeenCalled()
  })
})
