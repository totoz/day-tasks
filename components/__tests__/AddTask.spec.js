import Vue from 'vue'
import Vuetify from 'vuetify'
import Notify from 'vue-notification'
import AddTask from '@/components/AddTask.vue'
import Vuex from 'vuex'
import { getModule } from 'vuex-module-decorators'
import TasksStore from '~/store/modules/tasks'
import { shallowMount, createLocalVue } from '@vue/test-utils'

Vue.use(Vuetify)
Vue.use(Notify)

const localVue = createLocalVue()
localVue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    tasks: TasksStore,
  },
})

const tasksStore = getModule(TasksStore, store)

let wrapper = null
let vuetify = new Vuetify()
const closeAddTask = jest.spyOn(
  AddTask.prototype.constructor.options.methods,
  'closeAddTask'
)
const createTask = jest.spyOn(
  AddTask.prototype.constructor.options.methods,
  'createTask'
)
const addLabelToTask = jest.spyOn(
  AddTask.prototype.constructor.options.methods,
  'addLabelToTask'
)

describe('TaskForm.vue', () => {
  beforeEach(() => {
    wrapper = shallowMount(AddTask, {
      propsData: {
        title: 'Test',
        task: 'Tarea 1',
      },
      data: () => {
        return {
          labels: [
            {
              id: '1234',
              color: '#fff',
              name: 'Dev',
            },
          ],
        }
      },
      vuetify,
      localVue,
      store,
    })
  })

  afterEach(() => {
    wrapper.destroy()
    jest.clearAllMocks()
  })

  it('should renders the vue instance', () => {
    expect(wrapper.findComponent(AddTask).vm).toBeTruthy()
  })

  it('should renders the title and the task', () => {
    const titleInput = wrapper.find("[data-testid='title-input']")
    const taskTextarea = wrapper.find("[data-testid='task-textarea']")
    expect(titleInput.props().value).toMatch('Test')
    expect(taskTextarea.props().value).toMatch('Tarea 1')
  })

  it('should close the dialog when cancel is clicked', () => {
    const closeBtn = wrapper.find("[data-testid='close-btn']")
    closeBtn.vm.$emit('click')
    expect(closeAddTask).toHaveBeenCalledTimes(1)
  })

  it('should create the new task confirm button is clicked', async () => {
    const confirmBtn = wrapper.find("[data-testid='confirm-btn']")
    confirmBtn.vm.$emit('click')
    await wrapper.vm.$nextTick()
    wrapper.vm.$emit('reset')
    wrapper.vm.$emit('close')
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted().reset).toHaveLength(1)
    expect(wrapper.emitted().close).toHaveLength(1)
    expect(createTask).toHaveBeenCalledTimes(1)
  })

  it('should add new label to the task', () => {
    const task = {
      id: '123',
      color: '#fff',
      name: 'Dev',
    }
    wrapper.vm.addLabelToTask(task)
    expect(addLabelToTask).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.$data.labels).toHaveLength(2)
  })

  it("shouldn't add the label to the task", () => {
    const task = {
      id: '1234',
      color: '#fff',
      name: 'Dev',
    }
    wrapper.vm.addLabelToTask(task)
    expect(addLabelToTask).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.$data.labels).toHaveLength(0)
  })
})
