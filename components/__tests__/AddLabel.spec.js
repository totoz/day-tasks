import Vue from "vue";
import Vuetify from "vuetify";
import Notify from "vue-notification";
import AddLabel from "@/components/AddLabel.vue";
import { shallowMount } from "@vue/test-utils";

Vue.use(Vuetify);
Vue.use(Notify);

let wrapper = null;
let vuetify = new Vuetify();
const createLabel = jest.spyOn(AddLabel.prototype.constructor.options.methods, "createLabel");

describe("TaskForm.vue", () => {
  beforeEach(() => {
    wrapper = shallowMount(AddLabel, {
      computed: {
        labels: () => [],
      },
      vuetify,
    });
  });

  afterEach(() => {
    wrapper.destroy();
    jest.clearAllMocks();
  });

  it("should renders the vue instance", () => {
    expect(wrapper.findComponent(AddLabel).vm).toBeTruthy();
  });

  it("should show the add some labels message", () => {
    const addSomeLabels = wrapper.find("[data-testid='no-labels']");
    expect(addSomeLabels.exists()).toBeTruthy();
  });

  it("shouldn't show the add some labels message", () => {
    wrapper = shallowMount(AddLabel, {
      computed: {
        labels: () => [
          {
            id: "123",
            color: "#fff",
            name: "Dev",
          },
        ],
      },
      vuetify,
    });
    const addSomeLabels = wrapper.find("[data-testid='no-labels']");
    expect(addSomeLabels.exists()).toBeFalsy();
  });

  it("should add the label to the task", async () => {
    wrapper = shallowMount(AddLabel, {
      computed: {
        labels: () => [
          {
            id: "123",
            color: "#fff",
            name: "Dev",
          },
        ],
      },
      vuetify,
    });
    const labelCheckbox = wrapper.find("[data-testid='label-checkbox']");
    labelCheckbox.vm.$emit("change");
    await wrapper.vm.$nextTick();
    expect(wrapper.emitted().addLabel).toHaveLength(1);
  });

  it("should create a new label", async () => {
    const addLabelExpand = wrapper.find("[data-testid='addLabel-expand']");
    addLabelExpand.vm.$emit("click");
    await wrapper.vm.$nextTick();
    const createLabelBtn = wrapper.find("[data-testid='createLabel-btn']");
    createLabelBtn.vm.$emit("click");
    await wrapper.vm.$nextTick();
    expect(createLabel).toHaveBeenCalledTimes(1);
  });
});
