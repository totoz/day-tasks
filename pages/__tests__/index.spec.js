import Vue from 'vue'
import Vuetify from 'vuetify'
import Tasks from '@/pages/index.vue'
import { mount, createLocalVue } from '@vue/test-utils'
import Notify from 'vue-notification'

Vue.use(Vuetify)
Vue.use(Notify)

const localVue = createLocalVue()

describe('Index.vue', () => {
  let vuetify
  beforeEach(() => {
    vuetify = new Vuetify({
      theme: {
        dark: false,
      },
    })
  })

  it('loads the vue instance', () => {
    const wrapper = mount(Tasks, {
      computed: {
        dark: () => false,
        tasks: () => [],
      },
      stubs: {
        ClientOnly: true,
      },
      localVue,
      vuetify,
    })
    expect(wrapper.findComponent(Tasks).vm).toBeTruthy()
  })

  it('shows no result image', () => {
    const wrapper = mount(Tasks, {
      computed: {
        dark: () => false,
        tasks: () => [],
      },
      stubs: {
        ClientOnly: true,
      },
      localVue,
      vuetify,
    })
    expect(wrapper.find("[data-testid='no-results']").exists()).toBe(true)
  })

  it('renders the tasks ', () => {
    const wrapper = mount(Tasks, {
      computed: {
        dark: () => false,
        tasks: () => [{ id: '123', title: 'Test', task: 'Hacer test' }],
      },
      stubs: {
        ClientOnly: true,
        Task: true,
      },
      localVue,
      vuetify,
    })
    expect(wrapper.find("[data-testid='no-results']").exists()).toBe(false)
    expect(wrapper.vm.tasks).toHaveLength(1)
  })

  it('changes the theme ', () => {
    const wrapper = mount(Tasks, {
      computed: {
        dark: {
          get() {
            return this.$vuetify.theme.dark
          },
          set(newValue) {
            this.$vuetify.theme.dark = newValue
          },
        },
        tasks: () => [{ id: '123', title: 'Test', task: 'Hacer test' }],
      },
      stubs: {
        ClientOnly: true,
        Task: true,
      },
      localVue,
      vuetify,
    })

    wrapper.find("[data-testid='dark-mode']").trigger('click')
    expect(wrapper.vm.$vuetify.theme.dark).toBe(true)
  })
})
