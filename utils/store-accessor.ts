import { Store } from 'vuex'
import { getModule } from 'vuex-module-decorators'
import TasksStore from '~/store/tasks'
import LabelsStore from '~/store/labels'

let tasksStore: TasksStore
let labelsStore: LabelsStore

function initialiseStores(store: Store<any>): void {
  tasksStore = getModule(TasksStore, store)
  labelsStore = getModule(LabelsStore, store)
}

export { initialiseStores, tasksStore, labelsStore }
